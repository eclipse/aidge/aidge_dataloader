#include <cassert>

#include "aidge/data/Dataloader.hpp"

using namespace Aidge; 

Dataloader::Dataloader(Database& database, unsigned int batchSize, bool shuffle, bool dropLast)
    :
    mDatabase(database),
    mBatchSize(batchSize),
    mShuffle(shuffle),
    mDropLast(dropLast)
{
    // We assume all data & labels have the same size, backend & datatype 
    mDataSize = (mDatabase.get_item(0).first)->dims();
    mDataBackend = (mDatabase.get_item(0).first)->getImpl()->backend();
    mDataType = (mDatabase.get_item(0).first)->dataType();
    mLabelSize = (mDatabase.get_item(0).second)->dims();
    mLabelBackend = (mDatabase.get_item(0).second)->getImpl()->backend();
    mLabelType = (mDatabase.get_item(0).second)->dataType();
}

std::pair<std::shared_ptr<Tensor>,std::shared_ptr<Tensor>> Dataloader::readBatch(unsigned int startIndex)
{
    assert((startIndex) <= mDatabase.get_len() && "readBatch : dataloader fetch out of bounds");
    
    std::vector<size_t> dataBatchSize = mDataSize;
    std::vector<size_t> labelBatchSize = mLabelSize;
    unsigned int current_batch_size;
    
    if ((startIndex+mBatchSize) > mDatabase.get_len()){
        current_batch_size = mDatabase.get_len()-startIndex;
    } else {
        current_batch_size = mBatchSize;     
    }
    dataBatchSize.insert(dataBatchSize.begin(), current_batch_size);
    labelBatchSize.insert(labelBatchSize.begin(), current_batch_size);
    

    std::shared_ptr<Tensor> batchData = std::make_shared<Tensor>();
    batchData->resize(dataBatchSize);
    batchData->setBackend(mDataBackend);
    batchData->setDatatype(mDataType);
    std::shared_ptr<Tensor> batchLabel = std::make_shared<Tensor>();
    batchLabel->resize(labelBatchSize);
    batchLabel->setBackend(mLabelBackend);
    batchLabel->setDatatype(mLabelType);

    for (unsigned int i = 0; i < current_batch_size; ++i){
        std::pair<std::shared_ptr<Tensor>, std::shared_ptr<Tensor>> rawDataLabel = mDatabase.get_item(startIndex+i); 
        const std::shared_ptr<Tensor> rawData = rawDataLabel.first;
        const std::shared_ptr<Tensor> rawLabel = rawDataLabel.second;
        
        // Assert data & label tensor sizes
        assert(rawData->dims() == mDataSize && "readBatch : corrupted Data size");
        assert(rawLabel->dims() == mLabelSize && "readBatch : corrupted Label size");
        
        // Assert data & label implementation backend
        assert(rawData->getImpl()->backend() == mDataBackend && "readBatch : corrupted data backend");
        assert(rawLabel->getImpl()->backend() == mLabelBackend && "readBatch : corrupted Label backend");

        // Assert data & label DataType
        assert(rawData->dataType() == mDataType && "readBatch : corrupted data DataType");
        assert(rawLabel->dataType() == mLabelType && "readBatch : corrupted Label DataType");

        // Concatenate into the batch tensor
        batchData->getImpl()->copy(rawData->getImpl()->rawPtr(), rawData->size(), i*rawData->size());
        batchLabel->getImpl()->copy(rawLabel->getImpl()->rawPtr(), rawLabel->size(), i*rawLabel->size());
    }
    return std::make_pair(batchData, batchLabel);
}
