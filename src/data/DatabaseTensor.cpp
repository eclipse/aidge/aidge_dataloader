#include <cassert>

#include "aidge/data/DatabaseTensor.hpp"

using namespace Aidge; 

DatabaseTensor::DatabaseTensor(bool train)
{
        mTrain = train;
}

void DatabaseTensor::add(std::shared_ptr<Tensor> data_input, std::shared_ptr<Tensor> label)
{
    mData.push_back(data_input);
    mLabel.push_back(label);
}

std::pair<std::shared_ptr<Tensor>,std::shared_ptr<Tensor>> DatabaseTensor::get_item(unsigned int index)
{
    assert(mData.size() == mLabel.size() && "DatabaseTensor data and label corrupted");
    assert(index < mData.size() && "DatabaseTensor get_item out of bounds");
    return std::make_pair(mData[index],mLabel[index]);
}
unsigned int DatabaseTensor::get_len(){
    assert(mData.size() == mLabel.size() && "DatabaseTensor data and label corrupted");
    return  mData.size();
}