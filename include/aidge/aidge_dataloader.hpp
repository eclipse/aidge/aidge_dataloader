/********************************************************************************
 * Copyright (c) 2023 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef __AIDGE_MODULE_DATALOADER__
#define __AIDGE_MODULE_DATALOADER__

#include "aidge/data/Database.hpp"
#include "aidge/data/DatabaseTensor.hpp"
#include "aidge/data/Dataloader.hpp"

#endif /* __AIDGE_MODULE_DATALOADER__ */
