#ifndef Dataloader_H_
#define Dataloader_H_

#include "aidge/data/Database.hpp"
#include "aidge/data/Data.hpp"

namespace Aidge{


/**
 * @brief Data loader. Takes in a database and compose batches by fetching data from the given database.
 * @todo Implement readRandomBatch to compose batches from the database with a random sampling startegy. Necessary for training.
 */
class Dataloader {

public:
    /**
     * @brief Constructor of Data loader. 
     * @param database database from which to load the data
     * @param batchSize how many samples per batch to load
     * @param shuffle set to true to have the batch of data randomly sampled from the dataset
     * @param dropLast when the dataset size is not divisible by the batch size, set to true, the last incomplete batch will be dropped, set to false, the last incomplete batch is used.
     */
    Dataloader(Database& database, unsigned int batchSize, bool shuffle, bool dropLast);

    /**
     * @brief Create a batch of data and their corresponding ground truth from a starting index of the database
     * @param startIndex the starting database index to create the batch
     * @return A pair of pointers to the batch of data (first) and the batch of ground truth (second)
     */
    std::pair<std::shared_ptr<Tensor>,std::shared_ptr<Tensor>> readBatch(unsigned int startIndex);

protected:

    // Dataset providing the data to the dataloader
    Database& mDatabase;
    std::vector<size_t> mDataSize;
    std::vector<size_t> mLabelSize;
    std::string mDataBackend;
    DataType mDataType;
    std::string mLabelBackend;
    DataType mLabelType;

    // Desired size of the produced batches
    unsigned int mBatchSize;
    
    bool mShuffle;
    
    // Drop the last batch if it contains less than the batch size
    bool mDropLast;

    

    // Vectors containing StimulusIDs from datasets
    //std::vector<unsigned int> mBatchsIndexes;
    
    // Queues containing indexes of the batchs
    //std::deque<unsigned int> mIndexes;

};

}

#endif /* Dataloader_H_ */