#ifndef DatabaseTensor_H_
#define DatabaseTensor_H_

#include "aidge/data/Tensor.hpp"
#include "aidge/data/Database.hpp"

namespace Aidge{

/**
 * @brief Database tensor. Database wrapping tensors.
 */
class DatabaseTensor : public Database {

public:
    /**
     * @brief Database tensor constructor.
     * @param train set to true if the data is training data. Set to False, it is considered as testing data.
     */
    DatabaseTensor(bool train);
    
    /**
     * @brief Add a data sample and its corresponding ground truth to the dataset.
     * @param data pointer to the data sample tensor
     * @param label pointer to the ground truth tensor corresponding to ```data``` 
     */
    void add(std::shared_ptr<Tensor> data, std::shared_ptr<Tensor> label);

    /**
     * @brief Fetch a data sample and its corresponding ground_truth 
     * @param index index of the pair (```data tensor```, ```ground truth tensor```) to fetch from the database
     * @return A pair of pointers to the data tensor (first) and its corresping ground truth tensor (second)
     */
    std::pair<std::shared_ptr<Tensor>,std::shared_ptr<Tensor>> get_item(unsigned int index) override;

    /** 
     * @return The number of data samples in the database.
     */
    unsigned int get_len() override;

    bool mTrain;

protected:
    
    std::vector<std::shared_ptr<Tensor>> mData;
    std::vector<std::shared_ptr<Tensor>> mLabel;

};

}

#endif /* DatabaseTensor_H_ */