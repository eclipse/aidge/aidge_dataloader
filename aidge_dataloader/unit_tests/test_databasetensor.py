import unittest
import aidge_core
import aidge_backend_cpu
import aidge_dataloader
import numpy as np


class test_databasetensor(unittest.TestCase):
    """Test tensor binding
    """
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_instanciation(self):
        val_data_aidge = aidge_dataloader.DatabaseTensor(False)
        data_size = 10
        true_np_batch_data = np.random.rand(data_size,3,224,224).astype(np.float32)
        true_np_batch_lbl = np.random.randint(0,1000,(data_size,1)).astype(np.int32)
        for i in range(data_size):
            # Numpy -> Tensor
            t_data = aidge_core.Tensor(true_np_batch_data[i])
            t_lbl = aidge_core.Tensor(true_np_batch_lbl[i])
            self.assertEqual(t_data.dtype(), aidge_core.DataType.Float32)
            self.assertEqual(t_lbl.dtype(), aidge_core.DataType.Int32)
            val_data_aidge.add(t_data, t_lbl)

        self.assertEqual(val_data_aidge.len(), data_size)

        for i in range(data_size):
            data,lbl = val_data_aidge.get_item(i)
            # Numpy automatically transforms batch_data into a numpy array
            self.assertTrue(np.allclose(data, true_np_batch_data[i]))
            self.assertTrue(np.array_equal(lbl, true_np_batch_lbl[i]))


if __name__ == '__main__':
    unittest.main()