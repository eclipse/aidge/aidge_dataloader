import unittest
import aidge_core
import aidge_backend_cpu
import aidge_dataloader
import numpy as np


class test_dataloader(unittest.TestCase):
    """Test tensor binding
    """
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_readbatch(self):
        
        # Create a databaseTensor
        val_data_aidge = aidge_dataloader.DatabaseTensor(False)
        
        # Dummy data & batch size settings
        data_size = 20
        batch_size = 5
        
        # Generate dummy data with numpy
        true_np_batch_data = np.random.rand(data_size,3,224,224).astype(np.float32)
        true_np_batch_lbl = np.random.randint(0,1000,(data_size,1)).astype(np.int32)
        print("generated numpy tensor")

        # Convert numpy array into tensors and add each tensor to the databaseTensor
        for i in range(data_size):
            print(i)
            # Numpy -> Tensor
            t_data = aidge_core.Tensor(true_np_batch_data[i])
            t_lbl = aidge_core.Tensor(true_np_batch_lbl[i])
            self.assertEqual(t_data.dtype(), aidge_core.DataType.Float32)
            self.assertEqual(t_lbl.dtype(), aidge_core.DataType.Int32)
            val_data_aidge.add(t_data, t_lbl)
        self.assertEqual(val_data_aidge.len(), data_size)

        # Create a dataloader
        val_dataloader_aidge = aidge_dataloader.Dataloader(val_data_aidge, batch_size, False, False)

        # Read batch & perform tests
        for i in range(0,data_size,batch_size):
            batch_data, batch_lbl = val_dataloader_aidge.read_batch(i)
            # Numpy automatically transforms batch_data into a numpy array
            offset = batch_size if ((i+batch_size) <= data_size) else (data_size-i)
            self.assertTrue(np.allclose(batch_data, true_np_batch_data[i:i+offset]))
            self.assertTrue(np.array_equal(batch_lbl, true_np_batch_lbl[i:i+offset]))

if __name__ == '__main__':
    unittest.main()