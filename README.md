# Aidge module template

You can copy and use this module template to build your own custom module for Aidge. Everything is already in place for adding new C++ and/or Python functionnalities and make them available to Aidge users as an add-on module.

## Steps

1) Edit your module name and version number in the [``project_name.txt``](project_name.txt) and [``version.txt``](version.txt) files respectively.

2) Add your C++ headers and sources in the [``include/aidge``](include/aidge) and [``src``](src) folders respectively.

    2.1. If you need Python binding for your C++ functions, you can add them in the [``python_binding``](python_binding) folder;

    2.2. You can add your unit tests, using [Catch2](https://github.com/catchorg/Catch2), in the [``unit_tests``](unit_tests) folder.

3) Add your pure Python sources in the [``aidge_module_template``](aidge_module_template) folder.

## Content

There are 3 independent basic examples embedded in this module template to help get you started:

- Example 1: a simple C++ function (first part in [header](include/aidge/example.hpp); [source](src/example.cpp)) extending the Aidge API that is also binded in Python. A simple C++ unit test is included as well ([source](unit_tests/Test.cpp));
- Example 2: an operator custom backend implementation in C++ (second part in [header](include/aidge/example.hpp); [source](src/example.cpp)) that is automatically registered in Aidge;
- Example 3: a pure Pyhon function ([source](aidge_module_template/example.py)) extending the Aidge API (Python only). A simple Python unit test is included as well ([source](aidge_module_template/tests/test_example.py)).


## Installation

### For C++ API

To build and install the C++ API of this module, create a ``build`` sub-directory and **inside** ``build`` run: 

```bash
cmake -DCMAKE_PREFIX_PATH=path/to/aidge/install/folder -DCMAKE_INSTALL_PREFIX:PATH=path/to/install/folder ..
make all install
```

Where ``path/to/aidge/install/folder`` is the install path of Aidge and the other Aidge modules and ``path/to/install/folder`` is the install path for your new module (which may be the same as the Aidge install folder).

**Compilation options**

|   Option   | Value type | Description |
|:----------:|:----------:|:-----------:|
| *-DCMAKE_INSTALL_PREFIX:PATH* | ``str``  | Path to the install folder |
| *-DCMAKE_BUILD_TYPE*          | ``str``  | If ``Debug``, compile in debug mode, ``Release`` compile with highest optimisations, default= ``Release`` |
| *-DWERROR*                    | ``bool`` | If ``ON`` show warning as error during compilation phase, default=``OFF`` |
| *-DPYBIND*                    | ``bool`` | If ``ON`` activate python binding, default=``ON`` |

If you have compiled with PyBind you can find at the root of the ``build`` file the python lib ``aidge_core.cpython*.so``

### For Python API

To build and install the Python API of this module on Linux using pip, follow those steps:

1. Create your python environnement with python >= 3.7. For example using virtualenv:
``` bash
virtualenv -p python3.8 py_env_aidge
source py_env_aidge/bin/activate
```

2. Set the desired install path (which should be the same that you used for Aidge and other Aidge modules):
``` bash 
export AIDGE_INSTALL = '<path_to_aidge>/install'
```

3. Build your ``aidge_module_template`` module:
``` bash
pip install . -v
```


## Run tests

### CPP

Inside of the build folder, run:

```bash
ctest --output-on-failure
```

### Python

Inside the ``aidge_module_template`` sub-folder, run:

```bash
python -m unittest discover -s tests -v
```
