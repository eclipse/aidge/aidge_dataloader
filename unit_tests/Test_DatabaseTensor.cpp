#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/data/DatabaseTensor.hpp"
#include "aidge/aidge_backend_cpu.hpp"
#include "aidge/data/TensorImpl.hpp"

using namespace Aidge;

TEST_CASE("DatabaseTensor instanciation  & fill ") {

    std::shared_ptr<Tensor> x1 = std::make_shared<Tensor>(Array1D<int,4> {{7,0,9,0}});
    std::shared_ptr<Tensor> y1 = std::make_shared<Tensor>(Array1D<int,1> {{1}});

    std::shared_ptr<Tensor> x2 = std::make_shared<Tensor>(Array1D<int,4> {{2,6,8,2}});
    std::shared_ptr<Tensor> y2 = std::make_shared<Tensor>(Array1D<int,1> {{2}});

    
    DatabaseTensor D(true);

    D.add(x1,y1);
    D.add(x2,y2);

    REQUIRE(D.get_len() == 2);

    REQUIRE(D.get_item(0).first == x1);
    REQUIRE(D.get_item(0).second == y1);

    REQUIRE(D.get_item(1).first == x2);
    REQUIRE(D.get_item(1).second == y2);
}
