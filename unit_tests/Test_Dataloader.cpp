#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/data/DatabaseTensor.hpp"
#include "aidge/data/Dataloader.hpp"
#include "aidge/aidge_backend_cpu.hpp"
#include "aidge/data/TensorImpl.hpp"

using namespace Aidge;

TEST_CASE("Dataloader instanciation  & readBatch") {
    
    // Create database
    bool train = false;
    DatabaseTensor D(train);

    // Dummy data settings
    unsigned int samples = 20;
    unsigned int batchSize = 5;
    unsigned int number_batch = samples / batchSize;

    //Generate dummy data for the database
    for (int i = 0; i < samples; ++i){
        std::shared_ptr<Tensor> x1 = std::make_shared<Tensor>(Array2D<int,2,2> {{{2*i,3*i},{5*i,7*i}}});
        std::shared_ptr<Tensor> y1 = std::make_shared<Tensor>(Array1D<int,1> {{i%3}});
        D.add(x1,y1);
    }
    
    // Instanciate the dataloader
    Dataloader loader(D, batchSize, false, false);

    // Perform the tests on the batches
    REQUIRE(D.get_len() == samples);

    for (unsigned int i = 0; i < number_batch; ++i){
        std::pair<std::shared_ptr<Tensor>,std::shared_ptr<Tensor>> batch = loader.readBatch(i*batchSize);
        std::shared_ptr<Tensor> data_batch = batch.first;
        data_batch->print();
        std::shared_ptr<Tensor> label_batch = batch.second;
        label_batch->print();
        
        for (unsigned int s = 0; s < batchSize; ++s){
            std::shared_ptr<Tensor> data = D.get_item(i*batchSize+s).first;
            std::shared_ptr<Tensor> label = D.get_item(i*batchSize+s).second;
            unsigned int size_data = data->size();
            unsigned int size_label = label->size();
            for (unsigned int j = 0; j < size_data; ++j){
                REQUIRE(static_cast<int *>(data->getImpl()->rawPtr())[j] == static_cast<int *> (data_batch->getImpl()->rawPtr())[size_data*s+j]);
            }
            for (unsigned int j = 0; j < size_label; ++j){
                REQUIRE(static_cast<int *>(label->getImpl()->rawPtr())[j] == static_cast<int *> (label_batch->getImpl()->rawPtr())[size_label*s+j]);
            }
        }
    }
}