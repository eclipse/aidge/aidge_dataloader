Include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v3.0.1 # or a later release
)

FetchContent_MakeAvailable(Catch2)

file(GLOB_RECURSE src_files "*.cpp")

add_executable(tests${module_name} ${src_files})

target_link_libraries(tests${module_name} PUBLIC ${module_name})

target_link_libraries(tests${module_name} PRIVATE Catch2::Catch2WithMain)

list(APPEND CMAKE_MODULE_PATH ${catch2_SOURCE_DIR}/extras)
include(CTest)
include(Catch)
catch_discover_tests(tests${module_name})
