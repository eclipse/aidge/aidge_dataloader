#include <pybind11/pybind11.h>
#include "aidge/aidge_dataloader.hpp"

namespace py = pybind11;

namespace Aidge {
void init_DatabaseTensor(py::module&);
void init_Dataloader(py::module&);


PYBIND11_MODULE(aidge_dataloader, m) {
    init_DatabaseTensor(m);
    init_Dataloader(m);
}
}
