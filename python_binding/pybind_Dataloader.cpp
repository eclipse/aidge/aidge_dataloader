#include <pybind11/pybind11.h>
#include "aidge/data/Database.hpp"
#include "aidge/data/Dataloader.hpp"

namespace py = pybind11;
namespace Aidge {

void init_Dataloader(py::module& m){
    py::class_<Dataloader, std::shared_ptr<Dataloader>>(m, "Dataloader")
    .def(py::init<Database&, unsigned int, bool, bool>(), py::arg("database"), py::arg("batchSize"), py::arg("shuffle"), py::arg("dropLast"))
    .def("read_batch", &Dataloader::readBatch, py::arg("startIndex"))
    ;
}
  
}

