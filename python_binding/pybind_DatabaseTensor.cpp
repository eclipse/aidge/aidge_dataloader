#include <pybind11/pybind11.h>
#include "aidge/data/Database.hpp"
#include "aidge/data/DatabaseTensor.hpp"

namespace py = pybind11;
namespace Aidge {

void init_DatabaseTensor(py::module& m){

    py::class_<DatabaseTensor, std::shared_ptr<DatabaseTensor>, Database>(m, "DatabaseTensor")
    .def(py::init<bool>(), py::arg("train"))
    .def("add", &DatabaseTensor::add, py::arg("data"), py::arg("label"))
    .def("get_item", &DatabaseTensor::get_item, py::arg("index"))
    .def("len", &DatabaseTensor::get_len)
    ;
}
}